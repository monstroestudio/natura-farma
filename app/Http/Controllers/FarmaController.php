<?php

namespace App\Http\Controllers;

use App\Stores;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class FarmaController extends Controller
{
    protected $stores;

    /**
     * FarmaController constructor.
     * @param Stores $stores
     */
    public function __construct(Stores $stores)
    {
        $this->stores = $stores;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()->json(['data' => $this->stores->all()]);
    }

    /**
     * @return mixed
     */
    public function indexNull()
    {
        return $users = DB::table('stores')->where('latitude', '=', '')->orWhereNull('latitude')->get();
    }


    /**
     * @param Request $request
     * @return mixed
     */
    public function selectClosePosition(Request $request)
    {
        $latitude = ($request->latitude) ? $request->latitude : '-23.3188573';
        $longitude = ($request->longitude) ? $request->longitude : '-46.7644886';

        $stores = "SELECT * ,(3959 * acos(cos(radians('$latitude')) * cos(radians(latitude)) * cos(radians(longitude) - radians('$longitude')) + sin(radians('$latitude')) * sin(radians(latitude)))) AS distance
                    FROM stores
                    HAVING distance < 4
                    ORDER BY distance LIMIT 100";

        return DB::select(DB::raw($stores));
    }


    /**
     * @param Stores $store
     * @return Stores
     */
    public function show(Stores $store)
    {
        return $store;
    }


    /**
     * @param Stores $farma
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateFarma(Stores $farma, Request $request)
    {

        if ($farma->update($request->all())) {
            return response()->json(['status' => 'success']);
        };
        return response()->json(['status' => 'error', 'request' => $request->all()]);
        return response()->json($farma);

    }


    /**
     * @param Stores $store
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Stores $store, Request $request)
    {

        $store->latitude = $request->latitude;
        $store->longitude = $request->longitude;
        $store->google_data = $request->google_data;


        if ($request->address) {

        }

        if ($store->save()) {
            return response()->json(['status' => 'ok', 'id' => $store->id]);
        }
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findByZipCode(Request $request)
    {
        $code = $request->code;

        return response()->json($this->stores->where('zipCode', $code)->get(['latitude', 'longitude'])->first());
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function submit(Request $request)
    {
        $input = $request->all();
        $input['zipCode'] = str_replace('-','',$input['zipCode']);

        if ($this->stores->create($input)) {
            return response()->json(['status' => 'success']);
        };
        return response()->json(['status' => 'error', 'request' => $request->all()]);
    }


    /**
     * @param Stores $farma
     * @return View
     */
    public function edit(Stores $farma)
    {
        $farma->zipCode = str_replace('-', '', $farma->zipCode);
        return view('admin.edit', $farma->toArray());
    }


}
