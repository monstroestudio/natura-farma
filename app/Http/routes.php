<?php
header("Access-Control-Allow-Origin: *");

Route::get('/application', function () {
    return view('application');
});

Route::get('/', function () {
    return view('map');
});

Route::group(['prefix' => 'admin', 'middleware'=> 'auth'], function () {

    Route::get('/home', 'HomeController@index');

    Route::any('/', function () {
        return view('admin.list');
    });

    Route::any('/addFarma', function () {
        return view('admin.addFarma');
    });

    Route::any('/list', function () {
        return view('admin.list');
    });

    Route::any('/edit/{farma}', 'FarmaController@edit');
    Route::any('/edit/{farma}/submit', 'FarmaController@updateFarma');

    Route::any('/addFarma/submit', 'FarmaController@submit');
});

Route::group(['prefix' => 'api', 'middleware' => 'web'], function () {
    Route::get('/farma/empty', 'FarmaController@index');
    Route::get('/farma/near', 'FarmaController@selectClosePosition');
    Route::resource('/farma', FarmaController::class);
    Route::resource('/findByZipCode', 'FarmaController@findByZipCode');
});
