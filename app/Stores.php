<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stores extends Model
{
    protected $table = 'stores';
    protected $fillable = ['zipCode', 'fullAddress', 'latitude', 'longitude', 'name', 'neighbourhood', 'city', 'google_data'];
    protected $hidden = ['created_at', 'updated_at','google_data'];
}
