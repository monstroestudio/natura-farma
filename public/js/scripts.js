String.prototype.capitalizeFirstLetter = function () {
    return this.toLowerCase().charAt(0).toUpperCase() + this.slice(1);
}
var AppFarma = function () {
    var TOKEN = 'AIzaSyAT-HdXDOFRKk0dhX-9U2s-1UJ0A67MQFg';
    return {
        fitBounds: function () {
            if (AppFarma.markers.all.length > 1) {
                var bounds = new google.maps.LatLngBounds();
                $.each(AppFarma.markers.all, function (k, v) {
                    bounds.extend(v.getPosition())
                });
                AppFarma.map.fitBounds(bounds);
            }
        },
        addMarker: function (location, store) {
            var marker = new google.maps.Marker({
                position: location,
                map: AppFarma.map,
                icon: {
                    url: function () {
                        return 'img/pin-' + Math.floor((Math.random() * 3) + 1) + '.png'
                    }(),
                    size: new google.maps.Size(75, 103),
                    anchor: new google.maps.Point(63, 65)
                }
            });
            if (store) {
                AppFarma.markers[store].push(marker);
            }
            return marker;
        },
        removeAllMarkers: function () {
            $.each(AppFarma.markers, function (k, v) {
                $.each(AppFarma.markers[k], function (kk, vv) {
                    vv.setMap(null)
                    AppFarma.markers[k] = [];
                })
            })
            return this.markers;
        },
        searchPositions: {},
        markers: {
            'drogaraia': [],
            'drogasil': [],
            'all': [],
        },
        switchDrogaria: function (d) {
            if (d == "all") {
                $.each(AppFarma.markers['all'], function (k, v) {
                    v.setMap(AppFarma.map)
                });
                return;
            }
            $.each(['all', 'drogasil', 'drogaraia'], function (key, value) {
                if (value !== d) {

                    $.each(AppFarma.markers[value], function (k, v) {
                        v.setMap(null)
                    });
                } else {
                    // console.log('é que ->', value, ' é ->', d)

                    $.each(AppFarma.markers[d], function (k, v) {
                        v.setMap(AppFarma.map)
                    });
                }
            });
            return AppFarma.markers[d];
        },
        userCoords: {},
        getUserCoords: function (prefs) {
            //Inicia Mapa
            function myFitBounds(myMap, bounds) {
                myMap.fitBounds(bounds); // calling fitBounds() here to center the map for the bounds
                var overlayHelper = new google.maps.OverlayView();
                overlayHelper.draw = function () {
                    if (!this.ready) {
                        var extraZoom = getExtraZoom(this.getProjection(), bounds, myMap.getBounds());
                        if (extraZoom > 0) {
                            myMap.setZoom(myMap.getZoom() + extraZoom);
                        }
                        this.ready = true;
                        google.maps.event.trigger(this, 'ready');
                    }
                };
                overlayHelper.setMap(myMap);
            }
            function getExtraZoom(projection, expectedBounds, actualBounds) {
                // in: LatLngBounds bounds -> out: height and width as a Point
                function getSizeInPixels(bounds) {
                    var sw = projection.fromLatLngToContainerPixel(bounds.getSouthWest());
                    var ne = projection.fromLatLngToContainerPixel(bounds.getNorthEast());
                    return new google.maps.Point(Math.abs(sw.y - ne.y), Math.abs(sw.x - ne.x));
                }
                var expectedSize = getSizeInPixels(expectedBounds),
                    actualSize = getSizeInPixels(actualBounds);
                if (Math.floor(expectedSize.x) == 0 || Math.floor(expectedSize.y) == 0) {
                    return 0;
                }
                var qx = actualSize.x / expectedSize.x;
                var qy = actualSize.y / expectedSize.y;
                var min = Math.min(qx, qy);
                if (min < 1) {
                    return 0;
                }
                return Math.floor(Math.log(min) / Math.LN2);
            }
            function setMarkers(userCoords){
                AppFarma.removeAllMarkers();
                $.get('https://dpz.naturafarma.monstroestudio.com.br/api/farma/near?latitude=' + userCoords.lat + '&longitude=' + userCoords.lng ).then(function (stores) {
                    var infowindow = new google.maps.InfoWindow();
                    $.each(stores, function (k, store) {
                        var storeName = (store.name.toLowerCase() == "drogasil") ? 'drogasil' : 'drogaraia';
                        if (typeof prefs.showOnly !== "undefined" && prefs.showOnly == storeName && prefs.showOnly !== "all") return;
                        var latLong = {lat: parseFloat(store.latitude), lng: parseFloat(store.longitude)}
                        var marker = new google.maps.Marker({
                            position: latLong,
                            map: map,
                            title: '',
                            icon: {
                                url: function () {
                                    return 'img/pin-' + Math.floor((Math.random() * 3) + 1) + '.png'
                                }(),
                                size: new google.maps.Size(75, 103),
                                anchor: new google.maps.Point(63, 65)
                            }
                        });
                        AppFarma.markers[storeName].push(marker);
                        AppFarma.markers['all'].push(marker);
                        var contentString = '<div class="popup-content">' +
                            '<div id="siteNotice">' +
                            '<img src="img/logo-' + storeName + '.png" alt="" style="margin-top:5px"></div>' +
                            '<div id="bodyContent">' +
                            '<p style="text-transform: capitalize; margin: 2px 0 2px"> ' + store.fullAddress.capitalizeFirstLetter() + ' </p>' +
                            '<p> ' + store.neighbourhood.capitalizeFirstLetter() + ' - ' + store.city.capitalizeFirstLetter() + '  </p>' +
                            '</div>' +
                            '</div>';
                        google.maps.event.addListener(infowindow, 'domready', function () {
                            var iwOuter = $('.gm-style-iw');
                            var iwCloseBtn = iwOuter.next();
                            iwCloseBtn.css({
                                opacity: '1',
                                right: '10px',
                                top: '-5px',
                                width: '20px',
                                height: '21px',
                                border: 'none',
                                borderRadius: '13px',
                                'background-color': '#d9534f',
                                'background': '#d9534f',
                                'fontSize': '15px',
                                paddingLeft: '7px',
                                color: 'white',
                                paddingTop: '0px'
                            });
                            iwCloseBtn.find('img').remove();
                            iwCloseBtn.html('x');
                            iwCloseBtn.mouseout(function () {
                                $(this).css({opacity: '1'});
                            });
                            iwOuter.prev().children(':nth-child(2)').css({'width': '26px'});
                            var iwBackground = iwOuter.prev();
                            iwBackground.children(':nth-child(2)').css({'display': 'none'});
                            iwBackground.children(':nth-child(3)').find('div').children().css({
                                'box-shadow': 'rgba(0, 0, 0, 0.5) 1px 2px 27px',
                                'z-index': '1'
                            });
                            iwBackground.children(':nth-child(4)').css({
                                'border-radius': '20px',
                                'box-shadow': '10px 10px 30px rgba(0,0,0,0.3)'
                            });

                        });
                        google.maps.event.addListener(marker, 'click', function () {
                            infowindow.close(map, marker);
                            infowindow.setContent(contentString)
                            infowindow.open(map, this)
                        });
                        google.maps.event.addListener(AppFarma.map, 'zoom_changed', function () {
                            zoomChangeBoundsListener =
                                google.maps.event.addListener(map, 'bounds_changed', function(event) {
                                    if (this.getZoom() > 12 && this.initialZoom == true) {
                                        // Change max/min zoom here
                                        this.setZoom(12);
                                        this.initialZoom = false;
                                    }
                                    google.maps.event.removeListener(zoomChangeBoundsListener);
                                });
                        });
                    });
                });
            }
            function setCenter(userCoords){
                map.setCenter(userCoords);
                new google.maps.Marker({
                    position: userCoords,
                    map: map,
                    title: 'Voce está aqui!',
                    icon: 'img/user-position.png'
                });
            }
            //Ponto padrão para iniciar o mapa, praça da sé
            var map = AppFarma.map = new google.maps.Map(document.getElementById('map'), {
                zoom: (typeof AppFarma.map !== "undefined") ? AppFarma.map.zoom : 12,
                center : new google.maps.LatLng(-23.5498991, -46.6339552),
            });
            myLatLng = new Object();
            myLatLng = {
                lat: -23.5498991,
                lng: -46.6339552
            };
            setMarkers(myLatLng);
            google.maps.event.addListener(AppFarma.map, 'dragend', function (e) {
                $('#input-text').val('');
                var latLng = JSON.parse(JSON.stringify(AppFarma.map.getCenter()));
               setMarkers(latLng);
                // AppFarma.map.setZoom(zoom);
            });
            //Verifica o ip na tag meta do html
            if(navigator.geolocation){
                //Localização do usuário
                navigator.geolocation.getCurrentPosition(successPosition, showError);
            }
            function successPosition(position) {
                AppFarma.userCoords = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };
                setMarkers(AppFarma.userCoords);
                setCenter(AppFarma.userCoords);
                AppFarma.fitBounds();
                return;
            }
            function showError() {
                $.get('https://ipapi.co/json/').then(function (data) {
                    AppFarma.userCoords = {
                        lat: data.latitude,
                        lng: data.longitude
                    };
                    setMarkers(AppFarma.userCoords);
                    setCenter(AppFarma.userCoords);
                    AppFarma.fitBounds();
                    return;
                });
            }
        },
        init: function () {
            var $box = $('#search-box'),
                $input = $box.find('#input-text'),
                $filter = $box.find('.storeFilter');
            $box.find('a').on('click', function (e) {
                e.preventDefault()
                AppFarma.map.setCenter(AppFarma.userCoords);
            })
            $input.mask('00000-000');
            $('.bt-busca-cep').on('click', function () {
                $('#search-box input[type=search]').focus();
                $("html, body").animate({scrollTop: 650}, 500);
            });
            $filter.on("change", function () {
                if ($(this).val() == "all") return AppFarma.switchDrogaria('all');
                return AppFarma.switchDrogaria($(this).val());
            });
            $input.on('keyup', function (e) {
                var el = $(this);

                if (el.val().length > 8) {
<<<<<<< HEAD
                    //console.log('Maior que 8, verificando se há na database...')
                    $.get('https://dpz.naturafarma.monstroestudio.com.br/api/findByZipCode', {code: el.val()}).then(function (data) {
=======
                    $.get('/api/findByZipCode', {code: el.val()}).then(function (data) {
>>>>>>> dbe903c2c8efa4405abf343423fa3c6dc5922903
                        if (typeof data.latitude !== "undefined" && typeof data.longitude !== "undefined") {
                            AppFarma.removeAllMarkers();
                            //
                            //$.each(AppFarma.markers, function(k,v){
                            //    AppFarma.markers[k] = [];
                            //})
                            AppFarma.searchPositions = data;
                            // console.warn('SIM! Encontrado no banco de dados', data)
                            //AppFarma.map.setCenter({gm     })
                            AppFarma.searchCriteria = {
                                lat: data.latitude,
                                lng: data.longitude,
                            }
                            //
                            //$.each(AppFarma.markers, function (k, v) {
                            //    v.setMap(null)
                            //})
                            //
                            AppFarma.getUserCoords({
                                showOnly: 'all',
                                area: {
                                    lat: data.latitude,
                                    lng: data.longitude,
                                }
                            });
                            // console.error('AQUI É PRA MUDAR, HEIN!')
                            var newMakers = [];
                            function addMarker(location) {
                                var marker = new google.maps.Marker({
                                    position: location,
                                    map: AppFarma.map
                                });
                                AppFarma.markers.push(marker);

                                return marker;
                            }
<<<<<<< HEAD
                            $.get('https://dpz.naturafarma.monstroestudio.com.br/api/farma/near?latitude=' + AppFarma.searchCriteria.lat + '&longitude=' + AppFarma.searchCriteria.lng).then(function (stores) {
                                // console.error('OLHA SÓ:')
=======

                            $.get('http://dpz.naturafarma.monstroestudio.com.br/api/farma/near?latitude=' + AppFarma.searchCriteria.lat + '&longitude=' + AppFarma.searchCriteria.lng).then(function (stores) {
>>>>>>> dbe903c2c8efa4405abf343423fa3c6dc5922903
                                $.each(AppFarma.markers, function (k, v) {
                                    AppFarma.markers[k] = [];
                                })

                                if (!stores || typeof stores !== "object" || stores == [] || stores.length < 1) {
                                    // console.warn('Oi, oi, oi,')
                                }
                                var infowindow = new google.maps.InfoWindow();

                                $.each(stores, function (k, store) {
                                    var storeName = (store.name.toLowerCase() == "drogasil") ? 'drogasil' : 'drogaraia';
                                    //    if (typeof prefs.showOnly !== "undefined" && prefs.showOnly == storeName && prefs.showOnly !== "all") return;
                                    var latLong = {lat: parseFloat(store.latitude), lng: parseFloat(store.longitude)}
                                    var currentMarker = AppFarma.addMarker(latLong);
                                    AppFarma.markers[storeName].push(currentMarker);
                                    //var marker = new google.maps.Marker({
                                    //    position: latLong,
                                    //    map: AppFarma.map,
                                    //    title: '',
                                    //    icon: 'img/pin-' + Math.floor((Math.random() * 3) + 1) + '.png',
                                    //});

                                    var contentString = '<div class="popup-content">' +
                                        '<div id="siteNotice">' +
                                        '<img src="img/logo-' + storeName + '.png" alt="" style="margin-top:5px"></div>' +
                                        '<div id="bodyContent">' +
                                            //'<p> ' + store.name + ' </p>' +
                                        '<p style="text-transform: capitalize; margin: 2px 0 2px"> ' + store.fullAddress.capitalizeFirstLetter() + ' </p>' +
                                        '<p> ' + store.neighbourhood.capitalizeFirstLetter() + ' - ' + store.city.capitalizeFirstLetter() + '  </p>' +
                                        '</div>' +
                                        '</div>';
                                    google.maps.event.addListener(infowindow, 'domready', function () {

                                        var iwOuter = $('.gm-style-iw');

                                        var iwCloseBtn = iwOuter.next();

                                        iwCloseBtn.css({
                                            opacity: '1',
                                            right: '10px',
                                            top: '-5px',
                                            width: '20px',
                                            height: '21px',
                                            border: 'none',
                                            borderRadius: '13px',
                                            'background-color': '#d9534f',
                                            'background': '#d9534f',
                                            'fontSize': '15px',
                                            paddingLeft: '7px',
                                            color: 'white',
                                            paddingTop: '0px'
                                        });

                                        iwCloseBtn.find('img').remove();
                                        iwCloseBtn.html('x');

                                        iwCloseBtn.mouseout(function () {
                                            $(this).css({opacity: '1'});
                                        });

                                        iwOuter.prev().children(':nth-child(2)').css({'width': '26px'})
//                                iwOuter.next().css({'background-color': 'red'})

                                        var iwBackground = iwOuter.prev();

                                        iwBackground.children(':nth-child(2)').css({'display': 'none'});
                                        iwBackground.children(':nth-child(3)').find('div').children().css({
                                            'box-shadow': 'rgba(0, 0, 0, 0.5) 1px 2px 27px',
                                            'z-index': '1'
                                        });

                                        iwBackground.children(':nth-child(4)').css({
                                            'border-radius': '20px',
//                                    'width':'260px',
                                            'box-shadow': '10px 10px 30px rgba(0,0,0,0.3)'
                                        });

                                    })
                                    google.maps.event.addListener(currentMarker, 'click', function () {
                                        infowindow.close(AppFarma.map, currentMarker);
                                        infowindow.setContent(contentString)
                                        infowindow.open(AppFarma.map, this)
                                    });

                                })
                                AppFarma.fitBounds()

                            })


                        } else {
                            //console.warn('NÃO! Vamos procurar no Google...')
                            url = 'https://maps.googleapis.com/maps/api/geocode/json?' +
                                'language=pt-BR&' +
                                'key=' + TOKEN + '&' +
                                'components=country:BR|postal_code:' + el.val();
                            // console.log(url);

                            $.get(url).then(function (data) {
                                if (data) {
                                    if (data.status == "OK") {
                                        AppFarma.removeAllMarkers();
                                        //$.each(AppFarma.markers, function(k,v){
                                        //    AppFarma.markers[k] = [];
                                        //})
                                        //
                                        AppFarma.map.setCenter({
                                            lat: data.results[0].geometry.location.lat,
                                            lng: data.results[0].geometry.location.lng,
                                        });
                                        AppFarma.searchCriteria = {
                                            lat: data.results[0].geometry.location.lat,
                                            lng: data.results[0].geometry.location.lng,
                                        }
<<<<<<< HEAD
                                        $.get('https://dpz.naturafarma.monstroestudio.com.br/api/farma/near?latitude=' + AppFarma.searchCriteria.lat + '&longitude=' + AppFarma.searchCriteria.lng).then(function (stores) {
                                            // console.error('OLHA SÓ:')
=======

                                        $.get('http://dpz.naturafarma.monstroestudio.com.br/api/farma/near?latitude=' + AppFarma.searchCriteria.lat + '&longitude=' + AppFarma.searchCriteria.lng).then(function (stores) {
                                          //   console.error('OLHA SÓ:')

>>>>>>> dbe903c2c8efa4405abf343423fa3c6dc5922903
                                            AppFarma.removeAllMarkers();
                                            //$.each(AppFarma.markers, function(k,v){
                                            //    AppFarma.markers[k] = [];
                                            //})
                                            var infowindow = new google.maps.InfoWindow();
                                            if (!stores || typeof stores !== "object" || stores == [] || stores.length < 1) {
                                                // console.warn('Oi, oi, oi,');
                                                swal('Nada encontrado', 'Náo há farmácias em um raio de 5km do CEP inserido. \n Tente novamente.', 'error');
                                                return;
                                            }
                                            $.each(stores, function (k, store) {
                                                var storeName = (store.name.toLowerCase() == "drogasil") ? 'drogasil' : 'drogaraia';
                                                // console.log(storeName)
                                                //    if (typeof prefs.showOnly !== "undefined" && prefs.showOnly == storeName && prefs.showOnly !== "all") return;
                                                var latLong = {
                                                    lat: parseFloat(store.latitude),
                                                    lng: parseFloat(store.longitude)
                                                }
                                                var currentMarker = AppFarma.addMarker(latLong, storeName);
                                                AppFarma.markers['all'].push(currentMarker);
                                                //var marker = new google.maps.Marker({
                                                //    position: latLong,
                                                //    map: AppFarma.map,
                                                //    title: '',
                                                //    icon: 'img/pin-' + Math.floor((Math.random() * 3) + 1) + '.png',
                                                //});
                                                var contentString = '<div class="popup-content">' +
                                                    '<div id="siteNotice">' +
                                                    '<img src="img/logo-' + storeName + '.png" alt="" style="margin-top:5px"></div>' +
                                                    '<div id="bodyContent">' +
                                                        //'<p> ' + store.name + ' </p>' +
                                                    '<p style="text-transform: capitalize; margin: 2px 0 2px"> ' + store.fullAddress.capitalizeFirstLetter() + ' </p>' +
                                                    '<p> ' + store.neighbourhood.capitalizeFirstLetter() + ' - ' + store.city.capitalizeFirstLetter() + '  </p>' +
                                                    '</div>' +
                                                    '</div>';
                                                google.maps.event.addListener(infowindow, 'domready', function () {
                                                    var iwOuter = $('.gm-style-iw');
                                                    var iwCloseBtn = iwOuter.next();
                                                    iwCloseBtn.css({
                                                        opacity: '1',
                                                        right: '10px',
                                                        top: '-5px',
                                                        width: '20px',
                                                        height: '21px',
                                                        border: 'none',
                                                        borderRadius: '13px',
                                                        'background-color': '#d9534f',
                                                        'background': '#d9534f',
                                                        'fontSize': '15px',
                                                        paddingLeft: '7px',
                                                        color: 'white',
                                                        paddingTop: '0px'
                                                    });
                                                    iwCloseBtn.find('img').remove();
                                                    iwCloseBtn.html('x');
                                                    iwCloseBtn.mouseout(function () {
                                                        $(this).css({opacity: '1'});
                                                    });
                                                    iwOuter.prev().children(':nth-child(2)').css({'width': '26px'})
//                                iwOuter.next().css({'background-color': 'red'})
                                                    var iwBackground = iwOuter.prev();
                                                    iwBackground.children(':nth-child(2)').css({'display': 'none'});
                                                    iwBackground.children(':nth-child(3)').find('div').children().css({
                                                        'box-shadow': 'rgba(0, 0, 0, 0.5) 1px 2px 27px',
                                                        'z-index': '1'
                                                    });
                                                    iwBackground.children(':nth-child(4)').css({
                                                        'border-radius': '20px',
//                                    'width':'260px',
                                                        'box-shadow': '10px 10px 30px rgba(0,0,0,0.3)'
                                                    });
                                                });
                                                google.maps.event.addListener(currentMarker, 'click', function () {
                                                    infowindow.close(AppFarma.map, currentMarker);
                                                    infowindow.setContent(contentString)
                                                    infowindow.open(AppFarma.map, this)
                                                });
                                            });
                                            AppFarma.fitBounds();
                                        })
                                    }
                                }
                            });
                        }
                    })
                }
            })


        },

        searchPositions: {},
        markers: {
            'drogaraia': [],
            'drogasil': [],
            'all': [],
        },

        switchDrogaria: function (d) {

            if (d == "all") {
                $.each(AppFarma.markers['all'], function (k, v) {
                    v.setMap(AppFarma.map)
                });
                return;
            }


            $.each(['all', 'drogasil', 'drogaraia'], function (key, value) {
                if (value !== d) {

                    $.each(AppFarma.markers[value], function (k, v) {
                        v.setMap(null)
                    });
                } else {
                  //   console.log('é que ->', value, ' é ->', d)

                    $.each(AppFarma.markers[d], function (k, v) {
                        v.setMap(AppFarma.map)
                    });
                }
            });
        },
    }
}
();

function initMap() {
    var prefs = new Object();
    prefs.ip = $('meta[name=ip]')[0].content;
    AppFarma.getUserCoords(prefs);
    AppFarma.init();
}
