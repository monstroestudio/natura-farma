;(function (window, document, $, undefined) {
    'use strict';

    String.prototype.capitalizeFirstLetter = function () {
        return this.toLowerCase().charAt(0).toUpperCase() + this.slice(1);
    }

    var AdminFarma = window.AdminFarma = (function () {
        var TOKEN = 'AIzaSyAT-HdXDOFRKk0dhX-9U2s-1UJ0A67MQFg',
            $form = $('#formAddFarma'),
            $zip = $form.find('#zipCode'),
            $address = $form.find('#address'),
            $neighbourhood = $form.find('#neighbourhood'),
            $city = $form.find('#city'),
            $state = $form.find('#state'),
            $name = $form.find('#name').val();


        var root = this;

        return {
            list: function () {
                console.warn('----- listagem');

                $(document).on('click', '#farmaList .edit', function (e) {
                    e.preventDefault();
                    window.location = '/admin/edit/' + $(this).data('farmaId');
                })

                $(document).ready(function () {
                    $('#farmaList').DataTable({
                        "language": {
                            sEmptyTable: "Nenhum registro encontrado",
                            sInfo: "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                            sInfoEmpty: "Mostrando 0 até 0 de 0 registros",
                            sInfoFiltered: "(Filtrados de _MAX_ registros)",
                            sInfoPostFix: "",
                            sInfoThousands: ".",
                            sLengthMenu: "_MENU_ resultados por página",
                            sLoadingRecords: "Carregando...",
                            sProcessing: "Processando...",
                            sZeroRecords: "Nenhum registro encontrado",
                            sSearch: "Pesquisar",
                            oPaginate: {
                                sNext: "Próximo",
                                sPrevious: "Anterior",
                                sFirst: "Primeiro",
                                sLast: "Último"
                            },
                            oAria: {
                                sSortAscending: ": Ordenar colunas de forma ascendente",
                                sSortDescending: ": Ordenar colunas de forma descendente"
                            }
                        },
                        "ajax": "/api/farma",
                        "columns": [
                            {"data": "name"},
                            {"data": "neighbourhood"},
                            {"data": "city"},
                            {"data": "zipCode"},
                            {"data": "fullAddress"},
                            {"data": "latitude"},
                        ],
                        "columnDefs": [
                            {
                                "render": function (data, type, row) {
                                    return '<button class="btn btn-info edit" data-farma-id="' + row.id + '">Editar</button>';
                                },
                                "targets": 5
                            }]
                    });
                });
            }, formDataUpdate: function () {
                var mapGambs = JSON.parse(JSON.stringify(AdminFarma.map.marker.getPosition()));

                return {
                    'zipCode': $form.find('#zipCode').val(),
                    'fullAddress': $form.find('#addressN').val(),
                    'name': $form.find('#name').val(),
                    'latitude': mapGambs.lat,
                    'longitude': mapGambs.lng
                }
            },
            formData: function () {
                var mapGambs = JSON.parse(JSON.stringify(AdminFarma.map.marker.getPosition()));

                return {
                    'zipCode': $form.find('#zipCode').val(),
                    'fullAddress': $form.find('#address').val(),
                    'name': $form.find('#name').val(),
                    'latitude': mapGambs.lat,
                    'longitude': mapGambs.lng,
                    'neighbourhood': $form.find('#neighbourhood').val(),
                    'city': $form.find('#city').val(),
                    'state': $form.find('#state').val()
                }
            },
            invalidZip: true,

            generateMap: function (zipCode, latitude, longitude) {
                var el = zipCode;
                var url;

                url = 'https://maps.googleapis.com/maps/api/geocode/json?' +
                    'language=pt-BR&' +
                    'key=' + TOKEN + '&' +
                    'components=country:BR|postal_code:' + el;

                if (latitude && longitude && zipCode == null) {
                    url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=' + latitude + ',' + longitude + '&key=' + TOKEN
                }


                console.warn('OLHA A URL AI, BRASIL', url)

                //if (zipCode.length > 7) {


                    $.get(url).then(function (data) {

                        if (data) {
                            if (data.status == "OK") {
                                AdminFarma.invalidZip = false;
                                $.each([$address, $neighbourhood, $city, $state, $name], function (k, v) {
                                    console.log($(v))
                                    $(v).val('')
                                });

                                var result = data.results[0];

                                var map = AdminFarma.map = new google.maps.Map(document.getElementById('map'), {
                                    zoom: 17,
                                    center: {
                                        lat: result.geometry.location.lat,
                                        lng: result.geometry.location.lng
                                    }
                                });

                                var marker = AdminFarma.map.marker = new google.maps.Marker({
                                    position: AdminFarma.map.center,
                                    map: map,
                                    draggable: true
                                });

                                marker.addListener('dragend', function () {
                                    //AdminFarma.map.marker.position = marker.getPosition();
                                })

                                // AdminFarma.map.marker.position

                                $address.val(result.formatted_address);

                                var index = 1;
                                if (typeof result.address_components[4] === "undefined") {
                                    $state.val(result.address_components[2].long_name);
                                    $city.val(result.address_components[1].long_name);
                                } else {
                                    $state.val(result.address_components[4].long_name);
                                    $city.val(result.address_components[3].long_name);
                                    $neighbourhood.val(result.address_components[1].long_name);
                                }

                                $('#formAddFarma').find('input').each(function (k, v) {
                                    $(v).parent().removeClass('has-error');
                                });
                            }else{
                                AdminFarma.invalidZip = true;

                                $form.find('.form-inline').addClass('has-error').on('click', function () {
                                    $(this).removeClass('has-error')
                                });

                                $form.find('#buttonSearch').prop('disabled',true);


                               return swal('ERRO!','CEP inválido. Por favor, tente novamente.', 'error');
                            }
                        }
                    });

                //}
            },

            edit: function () {
                var $zip = $form.find('#zipCode'),
                    $address = $form.find('#address');

                $zip.mask('00000-000');

                if ($zip.val().length !== 0) {
                    AdminFarma.generateMap($zip.val());
                }

                $zip.on('keyup', function (e) {
                    if($(this).val()>8) {
                        AdminFarma.generateMap($(this).val());
                    }
                });

                $form.on('submit', function (e) {
                    e.preventDefault();


                    if ($form.find('#name').val() === null) {
                        $form.find('#name').parent().addClass('has-error').on('click', function () {
                            $(this).removeClass('has-error')
                        });
                        return;
                    }


                    $.post('/admin/edit/' + document.location.pathname.split('/')[3] + '/submit', AdminFarma.formDataUpdate()).then(function (data) {
                        if (typeof data !== "undefined" && data.status == "success") {
                            return swal('Sucesso!', 'Alterações realizadas com sucesso', 'success')
                        }
                        return swal('Erro!', 'Infelizmente, ocorreu um erro ao adicionar a farmácia. \n Por favor, tente novamente.', 'error')
                    }).fail(function (a, b, c) {
                        return swal('Erro!', 'Infelizmente, ocorreu um erro ao adicionar a farmácia. \n Por favor, tente novamente.', 'error')
                    })
                })
            },

            init: function () {
                var $zip = $form.find('#zipCode'),
                    $address = $form.find('#address');

                $zip.mask('00000-000');

                $zip.on('keyup', function (e) {
                    if($(this).val().length>8) {
                        $form.find('#buttonSearch').prop('disabled',false);

                        AdminFarma.generateMap($(this).val());
                    }

                });
                $form.find('#buttonSearch').prop('disabled',true);
                $form.find('#buttonSearch').on('click', function(){
                    if($zip.val().length>7) {
                        AdminFarma.generateMap($zip.val());
                    }
                })

                $form.on('submit', function (e) {
                    e.preventDefault();

                    var err = [];
                    $('#formAddFarma').find('input,select').each(function (k, v) {
                        if (v.value === "" || $(v).val() === null) {
                            err.push(v)
                            $(v).parent().addClass('has-error').on('click', function () {
                                $(this).removeClass('has-error')
                            });
                        }
                    });


                    if (err.length === 0 && AdminFarma.invalidZip !== true) {
                        $.post('/admin/addFarma/submit', AdminFarma.formData()).then(function (data) {
                            if (typeof data !== "undefined" && data.status == "success") {
                                $.each([$zip, $address, $neighbourhood, $city, $state, $name], function (k, v) {
                                    $(v).val('')
                                });
                                return swal('Sucesso!', 'Farmácia adicionada com sucesso', 'success');

                            }
                            return swal('Erro!', 'Infelizmente, ocorreu um erro ao adicionar a farmácia. \n Por favor, tente novamente.', 'error')
                        }).fail(function (a, b, c) {
                            return swal('Erro!', 'Infelizmente, ocorreu um erro ao adicionar a farmácia. \n Por favor, tente novamente.', 'error')
                        })
                    }else if(AdminFarma.invalidZip === true){
                        $form.find('.form-inline').addClass('has-error').on('click', function () {
                            $(this).removeClass('has-error')
                        });

                        $form.find('#buttonSearch').prop('disabled',true);

                        return swal('ERRO!','CEP inválido. Por favor, tente novamente.', 'error');
                    }else{
                        return false;
                    }
                })
            }
        }
    })();

//
//$(function () {
//});

})
(window, document, jQuery);