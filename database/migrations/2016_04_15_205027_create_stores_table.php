<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('name');
            $table->string('address');
            $table->string('neighbourhood');
            $table->string('city');
            $table->string('zipCode');
            $table->string('fullAddress');
            $table->string('latitude');
            $table->string('longitude');
            $table->string('google_data');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stores');
    }
}
