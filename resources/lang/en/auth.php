<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Usuário ou senha inválidos. Tente novamente.',
    'throttle' => 'Você tentou se logar várias vezes. Por faver, tente novamente em :seconds segundos.',

];
