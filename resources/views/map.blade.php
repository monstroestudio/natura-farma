<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="ip" content="{{$ip = getenv('HTTP_CLIENT_IP')?:
        getenv('HTTP_X_FORWARDED_FOR')?:
        getenv('HTTP_X_FORWARDED')?:
            getenv('HTTP_FORWARDED_FOR')?:
                getenv('HTTP_FORWARDED')?:
                    getenv('REMOTE_ADDR')}}">

    <title>Natura Farma</title>

    <link rel="stylesheet" href="/bower_components/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="/bower_components/font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" href="/bower_components/sweetalert2/dist/sweetalert2.css">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<div class="">

    <div class="container-map">
        <!-- <div id="header">
            <div class="header-top"></div>
            <a href="#" class="bt-busca-cep"></a>
            <a href="http://rede.natura.net/espaco/rede/sou-cat370002" target="_blank" title="Conheça toda a linha SOU" class="bt-conheca"></a>
            <p class="txt-comprar">
                Agora você pode comprar<br>
                toda linha SOU nas farmácias<br>
                <strong>DrogaRaia e Drogasil*.</strong>
            </p>
            <a href="http://rede.natura.net/espaco/conheca-SOU" target="_blank" class="bt-saiba-mais"></a>
            <div class="produto-amassado"></div>
            <div class="texto-compre"></div>
            <div class="texto-o-bom"></div>
        </div> -->

        <div class="header-top">
            <div class="intro">
                <div class="container">
                    <img src="img/logo.png" class="logo" alt="SOU">
                    <div class="compre-online text-right">
                        <div>
                            <span class="vermelho" style="display: block;padding-top: 9px;">Compre online</span>
                            <span class="laranja">ou nas farmácias</span>
                        </div>
                    </div>
                    <div class="poder-escolher">
                        <p class="escolha-melhor">
                            <span class="vermelho">Cuidado e beleza</span><br>
                            <span class="laranja">até a última gota</span>
                        </p>

                        <img src="img/sou1.png" class="sou1" alt="SOU">

                        <p class="comprar-linha-sou">
                            Agora você pode comprar<br />
                            as Máscaras Express SOU<br />
                            nas drograrias Droga Raia<br />
                            e Drogasil.
                        </p>

                        <p class="comprar-linha-sou-mobile">
                            Agora você pode comprar toda linha SOU nas drogarias <strong>Droga Raia e Drogasil*</strong>.
                        </p>


                        <p class="obs">
                            *À venda nas drogarias Droga Raia
                            e Drogasil em todo estado de São
                            Paulo. Outros estados do Brasil
                            a partir do 2° semestre/2016.
                        </p>

                    </div>

                    <img src="img/img-produto-vermelho.png" class="sou2" alt="SOU">
                </div>
            </div>
        </div>

        <div id="search-box">

            <div class="search-input">
                <i class="fa fa-search"></i>
                <input type="search" placeholder="Digite o seu CEP aqui" id="input-text">
                <a href="">Não sei meu CEP</a>
            </div>

            <div class="search-input-radio">
                <input type="radio" class="storeFilter custom-radio" name="storeFilter" id="all" value="all"
                       checked="checked"/>
                <label for="all" class="custom-label">Todas as farmácias</label>
                <br>
                <input type="radio" class="storeFilter custom-radio" name="storeFilter" id="drogaraia"
                       value="drogaraia"/>
                <label for="drogaraia" class="custom-label">Droga Raia</label>
                <br>
                <input type="radio" class="storeFilter custom-radio" name="storeFilter" id="drogasil" value="drogasil"/>
                <label for="drogasil" class="custom-label">Drogasil</label>
            </div>

            <!-- <div class="search-input-radio">
                <div class="radio">
                    <label for="">
                    <input type="radio" class="storeFilter" name="storeFilter" value="all" checked="checked">
                        Todas as farmácias
                    </label>
                </div>

                <div class="radio">
                    <label for="">
                    <input type="radio" class="storeFilter" name="storeFilter" value="drogaraia">
                        DrogaRaia
                    </label>
                </div>


                <div class="radio">
                    <label for="">
                    <input type="radio" class="storeFilter" name="storeFilter" value="drogasil">
                        Drogasil
                    </label>
                </div>

            </div> -->

        </div>
        <div id="map"></div>


    </div>

</div>
{{--swal('Nada encontrado', 'Náo há farmácias em um raio de 5km do CEP inserido. \n Tente novamente.', 'error')--}}
<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="/bower_components/sweetalert2/dist/sweetalert2.min.js"></script>
<script src="js/jquery.xdomainrequest.min.js"></script>
<script src="js/scripts.js"></script>

<script>

</script>
<!--

AIzaSyBdTYAerV2e6zz3kvNIW18cVex4T5PoIwI

-->
<script async defer
        src="https://maps.googleapis.com/maps/api/js?v=3.23&key=AIzaSyADrbmCy5YWNQWFlny9lkYeQVwgIsuMmw0&callback=initMap">
</script>
</body>
</html>
