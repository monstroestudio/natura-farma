@extends('layouts.admin')

@section('content')
<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="{{ url('/admin/addFarma') }}">Adicionar Farmácia</a></li>
                <li class="active"><a href="#">Listar Farmácias  <span class="sr-only">(atual)</span></a></li>
            </ul>





        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Listar Farmácias</h1>

            <div class="row">
                <div class="col-md-12">
                    <table id="farmaList" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Bandeira</th>
                            <th>Bairro</th>
                            <th>Cidade</th>
                            <th>Cep</th>
                            <th>Endereço Completo</th>
                            <th>Mapa</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Bandeira</th>
                            <th>Bairro</th>
                            <th>Cidade</th>
                            <th>Cep</th>
                            <th>Endereço Completo</th>
                            <th>Mapa</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>

        </div>
    </div>
@endsection
