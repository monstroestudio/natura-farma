@extends('layouts.admin')

@section('content')
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li class="active"><a href="#">Adicionar Farmácia <span class="sr-only">(atual)</span></a></li>
                <li><a href="list">Listar Farmácias</a></li>
            </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Adicionar Farmácia</h1>

            <div class="row">
                <div class="col-md-5">
                    <form role="form" id="formAddFarma">

                        <div class="form-inline" style="">
                            <div class="form-group">
                                <label class="sr-only">Digite o CEP</label>
                                <p class="form-control-static">Digite o CEP</p>
                            </div>
                            <div class="form-group">
                                <label for="inputPassword2" class="sr-only">Digite o CEP</label>
                                <input type="text" class="form-control" id="zipCode" placeholder="00000-000" size="25"
                                       maxlength="9">
                            </div>
                            <button type="button" class="btn btn-info" id="buttonSearch">Buscar</button>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Endereço</label>
                            <input type="text" class="form-control" id="address"
                                   placeholder="Entre com o endereço (nome da rua)" name="address">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Bairro</label>
                            <input type="text" class="form-control" id="neighbourhood" placeholder=""
                                   name="neighbourhood">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Cidade</label>
                            <input type="text" class="form-control" id="city" placeholder="" name="city">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Estado</label>
                            <input type="text" class="form-control" id="state" placeholder="" name="state">
                        </div>


                        <div class="form-group">
                            <label for="exampleInputPassword1">Rede:</label>

                            <select name="storeName" class="form-control" id="name">
                                <option selected disabled>Selecione a rede da farmácia</option>
                                <option value="RAIA">Droga Raia</option>
                                <option value="DROGASIL">Drogasil</option>
                            </select>
                        </div>

                        <button type="submit" class="btn btn-success">Adicionar farmácia</button>
                    </form>
                </div>

                <div class="col-md-6">
                    <div id="map">
                        <h2>Mapa</h2>
                        Digite o CEP para visualizar o mapa
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection