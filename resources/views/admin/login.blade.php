<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Natura Farma</title>

    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">
    <link href="/bower_components/sweetalert/themes/google/google.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">
    <link href="/bower_components/datatables/media/css/jquery.dataTables.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Natura Farma - ADMIN</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
        </div>
    </div>
</nav>

<div class="container-fluid">
    <div class="row">
        <div class="col-sm-3 col-md-2 sidebar">
            <ul class="nav nav-sidebar">
                <li><a href="addFarma">Adicionar Farmácia</a></li>
                <li class="active"><a href="#">Listar Farmácias  <span class="sr-only">(atual)</span></a></li>
            </ul>

        </div>
        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Listar Farmácias</h1>

            <div class="row">
                <div class="col-md-12">
                    <table id="farmaList" class="display" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Bandeira</th>
                            <th>Bairro</th>
                            <th>Cidade</th>
                            <th>Cep</th>
                            <th>Endereço Completo</th>
                            <th>Mapa</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Bandeira</th>
                            <th>Bairro</th>
                            <th>Cidade</th>
                            <th>Cep</th>
                            <th>Endereço Completo</th>
                            <th>Mapa</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>

            </div>

        </div>
    </div>
</div>


<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="/bower_components/datatables/media/js/jquery.dataTables.js"></script>
<script src="/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="/js/admin.js"></script>

<script>
    function initMap() {
        AdminFarma.init();
        AdminFarma.list();
    }
</script>


<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADrbmCy5YWNQWFlny9lkYeQVwgIsuMmw0&callback=initMap">
</script>

</body>
</html>
