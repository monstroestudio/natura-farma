<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Natura Farma</title>

    <link href='https://fonts.googleapis.com/css?family=Varela+Round' rel='stylesheet' type='text/css'>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/bower_components/sweetalert/dist/sweetalert.css" rel="stylesheet">


    <!-- Custom styles for this template -->
    <link href="/css/main.css" rel="stylesheet">
    <link href="/bower_components/datatables/media/css/jquery.dataTables.css" rel="stylesheet">

</head>

<body>

<nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar"
                    aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">Natura Farma - ADMIN</a>
        </div>
        <ul class="nav navbar-nav navbar-right">
            @if (Auth::guest())
                <li><a href="{{ url('/login') }}">Entrar</a></li>
                {{--<li><a href="{{ url('/register') }}">Register</a></li>--}}
            @else
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <ul class="dropdown-menu" role="menu">
                        <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Sair do sistema</a></li>
                    </ul>
                </li>
            @endif
        </ul>
        <div id="navbar" class="navbar-collapse collapse">
        </div>



    </div>
</nav>

<div class="container-fluid">
   @yield('content')
</div>


<script src="/bower_components/jquery/dist/jquery.min.js"></script>
<script src="/bower_components/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="/bower_components/datatables/media/js/jquery.dataTables.js"></script>
<script src="/bower_components/sweetalert/dist/sweetalert.min.js"></script>
<script src="/js/admin.js"></script>

<script>
    function initMap() {
        AdminFarma.init();
        AdminFarma.list();
    }
</script>


<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyADrbmCy5YWNQWFlny9lkYeQVwgIsuMmw0&callback=initMap">
</script>

</body>
</html>
